package com.accdata.app;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.accdata.app.config.PathConfig;
import com.accdata.app.config.PropertiesConfig;
import com.accdata.app.config.SpringConfig;
import com.accdata.db.embedded.EmbeddedDatabaseFactory;
import com.accdata.storage.entities.Person;
import com.accdata.storage.service.PersonService;
import com.accdata.utils.PropertiesConstants;

/**
 * 29-June-2015
 * 
 */

public class Main {

	private void loadEmbeddedDatabase() throws Exception {
		boolean isEmbeddedDbEnabled = PropertiesConfig
				.getBoolean(PropertiesConstants.PROPERTY_EMBEDDED_DB_ENABLED);
		if (isEmbeddedDbEnabled) {
			EmbeddedDatabaseFactory.getEmbeddedDatabase().start();
		}
	}

	public Main(String workingDirPath) throws IOException {
		PathConfig.setWorkingDirectory(new File(workingDirPath)
				.getAbsolutePath());
		System.setProperty(PropertiesConstants.PROPERTIES_APP_WORKING_DIR_PATH,
				workingDirPath);
		// setting external path to application for logback configuration XML
		// file
		System.setProperty(PropertiesConstants.PROPERTY_LOGBACK_CONFIG,
				PathConfig.getLogbackConfigPath());
		// System.setOut(new PrintStream(new LoggingOutputStream(AppLogger
		// .getSystemLogger(), "out"), true));
		// System.setErr(new PrintStream(new LoggingOutputStream(AppLogger
		// .getSystemLogger(), "error"), true));
	}

	public void boot() throws Exception {
		// load embedded database - if enabled :)
		loadEmbeddedDatabase();
	}

	public void shutdown() throws Exception {
		SpringConfig.getInstance().close();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	public static void main(String[] args) throws Exception {
		new Main("/Users/ajain/Documents/workspace/Hibernate-Spring").boot();
		System.out.println("*** Booted Successfully ***");
		System.out.println(PropertiesConfig
				.getString(PropertiesConstants.PROPERTY_LOGBACK_CONFIG_FILE));
		PersonService personService = (PersonService) SpringConfig
				.getInstance().getBean("personService");
		
		List<Person> persons = personService.readAll();
		for (Person person : persons) {
			personService.delete(person);
		}

		Person person = new Person();
		person.setName("YS Damn!");
		person.setEmail("ys@damn.it");
		try {
			personService.create(person);
			System.out.println("Person : " + person + " added successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<Person> nowpersons = personService.readAll();
		System.out.println("The list of all persons = " + nowpersons);
	}

}
