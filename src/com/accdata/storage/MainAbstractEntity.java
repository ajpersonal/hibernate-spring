package com.accdata.storage;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author aman
 */

@MappedSuperclass
public class MainAbstractEntity extends AbstractEntityImpl {

	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "sequence", strategy = "sequence", parameters = {
			@org.hibernate.annotations.Parameter(name = "sequenceName", value = "sequence"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.getClass().getSimpleName() + ":" + getId();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Entity) {
			Entity e = (Entity) obj;
			if (e.getId() == getId()) {
				return true;
			} else {
				return false;
			}
		} else {
			return super.equals(obj);
		}
	}

	@Override
	public int hashCode() {
		return (int) this.getId();

	}

}
