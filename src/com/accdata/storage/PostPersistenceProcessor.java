package com.accdata.storage;

/**
 * @author aman
 */
public interface PostPersistenceProcessor {

	public void postCreateOp();

	public void postUpdateOp();

	public void postDeleteOp();

}
