package com.accdata.storage;

/**
 * @author aman
 */

public interface PrePersistenceProcessor {

	public void preCreateOp();

	public void preUpdateOp();

	public void preDeleteOp();

}
