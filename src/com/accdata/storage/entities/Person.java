package com.accdata.storage.entities;

import java.io.StringWriter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.exception.ExceptionUtils;

import com.accdata.app.config.SpringConfig;
import com.accdata.storage.MainAbstractEntity;
import com.accdata.storage.service.PersonService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * @author aman
 */

@Entity
@Table(name = "PERSON")
@Path("persons")
public class Person extends MainAbstractEntity {

	private static final long serialVersionUID = -8487156716364715527L;

	private String name;
	private String email;
	private String phone;
	@Column(name = "ADDRESS")
	private String addr;

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Person [id=" + getId() + ", name=" + name + ", email=" + email + "]";
	}

	@Override
	public Response getEntity(UriInfo ui, HttpHeaders hh, String id) {
		Response.ResponseBuilder rb = null;
		try {
			Long entityId = Long.parseLong(id);
			StringWriter writer = new StringWriter();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
			PersonService personService = (PersonService) SpringConfig.getInstance().getBean("personService");
			Person person = personService.read(entityId);
			if (person == null)
				throw new Exception("Unable to find person object with id :" + entityId);
			mapper.writeValue(writer, person);
			rb = Response.ok(writer.toString());
		} catch (WebApplicationException ex) {
			throw ex;
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(ExceptionUtils.getRootCauseMessage(e)).type(MediaType.TEXT_PLAIN_TYPE).build());
		}
		return rb.build();

	}

	@Override
	public void preCreateOp() {
		System.out.println("**** Pre Create Op called for Person entity ****");
		// throw new RuntimeException("** Test Exception **");
	}

	@Override
	public void postCreateOp() {
		System.out.println("**** Post Create Op called for Person entity ****");
		// throw new RuntimeException("** Test Exception **");
	}

}
