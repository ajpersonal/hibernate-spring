package com.accdata.storage.dao.impl;

import org.springframework.stereotype.Repository;

import com.accdata.storage.dao.GenericDaoImpl;
import com.accdata.storage.entities.Person;

/**
 * @author aman
 */

@Repository
public class PersonDaoImpl extends GenericDaoImpl<Person, Long> implements PersonDao {

}
