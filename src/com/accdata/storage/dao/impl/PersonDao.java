package com.accdata.storage.dao.impl;

import com.accdata.storage.dao.GenericDao;
import com.accdata.storage.entities.Person;

/**
 * @author aman
 */

public interface PersonDao extends GenericDao<Person, Long> {

}