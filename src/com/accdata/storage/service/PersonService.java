package com.accdata.storage.service;

import org.springframework.stereotype.Component;

import com.accdata.storage.entities.Person;

/**
 * @author aman
 */

@Component
public class PersonService extends GenericEntityService<Person, Long> {

}