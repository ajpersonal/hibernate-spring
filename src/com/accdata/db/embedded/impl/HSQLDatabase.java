package com.accdata.db.embedded.impl;

import java.util.List;

import org.hsqldb.Server;

import com.accdata.app.config.PathConfig;
import com.accdata.app.config.PropertiesConfig;
import com.accdata.db.embedded.EmbeddedDatabase;
import com.accdata.utils.PropertiesConstants;

/**
 * 
 * @author aman
 */

public class HSQLDatabase implements EmbeddedDatabase {

	public static final String DB_FILE_PATH = "filePath";

	private static final String DB_PORT = "port";

	@Override
	public void start() throws Exception {
		// TODO Auto-generated method stub
		List<String> database = PropertiesConfig
				.getList(PropertiesConstants.PROPERTY_EMBEDDED_DB_HSQL_NAME);
		for (final String databaseName : database) {
			final String dbFilePath = PathConfig
					.getEmbeddedDbFilePath(databaseName);
			final int dbPort = PropertiesConfig
					.getInt(PropertiesConstants.PROPERTY_EMBEDDED_DB_HSQL_NAME
							+ "." + databaseName + "." + DB_PORT);
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						launch(dbFilePath, dbPort);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}).start();
		}

	}

	private void launch(String dbFilePath, int port) throws Exception {
		Server server = new Server();
		server.setDatabaseName(0, "");
		server.setDatabasePath(0, dbFilePath);
		server.setPort(port);
		server.setLogWriter(null);
		server.setSilent(true);
		server.setTrace(false);
		// start the HSQLDB in server mode
		server.start();
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		System.out.println("*** Shutdown DB ***");
	}

}
