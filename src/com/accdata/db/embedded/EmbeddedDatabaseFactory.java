package com.accdata.db.embedded;

import com.accdata.app.config.PropertiesConfig;
import com.accdata.db.embedded.impl.HSQLDatabase;

import static com.accdata.utils.PropertiesConstants.*;

/**
 * @author aman
 */

public class EmbeddedDatabaseFactory {

	public static EmbeddedDatabase getEmbeddedDatabase() throws Exception {
		EmbeddedDatabase db = null;
		String embeddedDbType = PropertiesConfig
				.getString(PROPERTY_EMBEDDED_DB_TYPE);
		if (EMBEDDED_HSQL_DB.equalsIgnoreCase(embeddedDbType)) {
			db = new HSQLDatabase();
		} else {
			throw new Exception("Not a valid embedded database type : \""
					+ embeddedDbType + "\"");
		}
		return db;
	}

}
